<?php

namespace console\controllers;

use console\models\News;
use console\models\Sender;
use console\models\Subscriber;
use yii\console\Controller;

class MailerController extends Controller
{
    public function actionSend()
    {
        $newsList = News::getList();
        $subscribers = Subscriber::getList();
        
        $result = Sender::run($subscribers, $newsList);
    }
    
    public function actionTest()
    {
        echo __METHOD__;
        die;
    }
}