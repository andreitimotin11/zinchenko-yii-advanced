<?php

namespace console\models;

class Sender
{
    public static function run($subscribers, $newsList)
    {
        foreach ($subscribers as $subscriber) {
            
            $result = \Yii::$app->mailer->compose()
                ->setFrom('test.php.up@gmail.com')
                ->setTo($subscriber['email'])
                ->setSubject('Тема сообщения')
                ->setTextBody('Text')
                ->setHtmlBody('<b>Text</b>')
                ->send();
        }
        
        return true;
    }
}