<?php

namespace console\models;

class Subscriber
{
    public static function getList()
    {
        $sql = 'SELECT * FROM subscriber';
        return \Yii::$app->db->createCommand($sql)->queryAll();
    }
}