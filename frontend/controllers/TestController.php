<?php

namespace frontend\controllers;

use frontend\models\Test;
use yii\web\Controller;

class TestController extends Controller
{
    public function actionIndex()
    {
        $list = Test::getNewsList(2);
        
        return $this->render('index', [
            'list' => $list,
        ]);
    }
    
    public function actionView($id = 1)
    {
        $item = Test::getItem($id);
        
        return $this->render('view', [
            'item' => $item,
        ]);
    }
}
