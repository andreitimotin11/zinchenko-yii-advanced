<?php

namespace frontend\models;

use frontend\components\StringHelper;

class Test
{
    /**
     * @param int $max
     * @return array
     * @throws \yii\db\Exception
     */
    public static function getNewsList($max)
    {
        $max = (int)$max;
        
        $sql = 'SELECT * FROM news LIMIT '. $max;
        
        $result =  \Yii::$app->db->createCommand($sql)->queryAll();
    
        if (!empty($result) && is_array($result)) {
            foreach ($result as &$item) {
                $item['content'] = \Yii::$app->stringHelper->getShort($item['content'], 1000);
            }
        }
        
        return $result;
    }
    
    /**
     * @param $id
     * @return array|false
     * @throws \yii\db\Exception
     */
    public static function getItem($id)
    {
        $id = (int) $id;
        $sql = 'SELECT * FROM news where news.id = '. $id;
    
        $result =  \Yii::$app->db->createCommand($sql)->queryOne();
        
        return $result;
    }
}