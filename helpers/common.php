<?php

if (!function_exists('dd')) {
    function dd($args)
    {
        if (is_array($args)) {
            foreach ($args as $item) {
                var_dump($item);
            }
        } else {
            var_dump($args);
        }
        die;
    }
}